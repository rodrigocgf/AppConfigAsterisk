#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <string.h>
#include "./../ConfigAsteriskLib/ConfigAsterisk.h"

void ShareLibraryCallOK();

int main(int argc, char **argv)
{
	void *lib_handle;
	static char * (*fn)(const char *);
	const char * psz_variable = "start";
	char *error;	
	char szScriptLua[10000];
	char *pszScriptLua;
	
	pszScriptLua = &szScriptLua[0];

	memset(szScriptLua,0x00,sizeof(szScriptLua));
	lib_handle = dlopen("/home/rodrigo/projects/c++/ConfigAsteriskLib/ConfigAsterisk.so", RTLD_LAZY);
	if (!lib_handle)
	{
		fprintf(stderr, "%s\n", dlerror());
		exit(1);
	}

	fn = dlsym(lib_handle, "getLuaScript");
	if ((error = dlerror()) != NULL) 
	{
		fprintf(stderr, "%s\n", error);
		exit(1);
	}
	
	pszScriptLua = (*fn)(psz_variable);	
	
	if ( pszScriptLua != NULL ) 
	{
		if ( strlen(pszScriptLua) < sizeof(szScriptLua) )
		{
			printf("[AppConfigAsterisk] returned SCRIPTLUA  : %s\r\n", pszScriptLua);
		}
		else
		{
			printf("szScriptLua size greater then sizeof(szScriptLua) \r\n");
		}
	} else
		printf("pszScriptLua returned is NULL !!! \r\n");
	
	
	
	dlclose(lib_handle);

	return 0;
}

void ShareLibraryCallOK()
{
	void *lib_handle;
	void (*fn)(const char * , char * pszScriptLua);
	char * psz_variable = "start";
	char *error;
	char szScriptLua[10000];

	memset(szScriptLua,0x00,sizeof(szScriptLua));
	lib_handle = dlopen("/home/rodrigo/projects/c++/ConfigAsteriskLib/ConfigAsterisk.so", RTLD_LAZY);
	if (!lib_handle)
	{
		fprintf(stderr, "%s\n", dlerror());
		exit(1);
	}

	fn = dlsym(lib_handle, "getLuaScript");
	if ((error = dlerror()) != NULL) 
	{
		fprintf(stderr, "%s\n", error);
		exit(1);
	}

	(*fn)(psz_variable , szScriptLua);
	printf("returned SCRIPTLUA  : %s\r\n", szScriptLua);
	
	dlclose(lib_handle);

	return 0;
}