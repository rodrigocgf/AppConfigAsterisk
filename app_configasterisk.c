
#include "asterisk.h"


#include "asterisk/options.h"
#include "asterisk/paths.h"
#include "asterisk/channel.h"
#include "asterisk/module.h"
#include "asterisk/lock.h"
#include "asterisk/app.h"
#include "asterisk/pbx.h"
#include "asterisk/utils.h"
#include "asterisk/hashtab.h"
#include "asterisk/cli.h"

#include <sys/stat.h>
#include <dlfcn.h>


#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <string.h>
#include "/home/rodrigo/projects/c++/ConfigAsteriskLib/ConfigAsterisk.h"

/*** DOCUMENTATION
        <application name="GetLuaScript" language="en_US">
                <synopsis>
                        Asterisk Database Configuration Reader module
                </synopsis>
                <syntax />
                <description>
                        <para>
							Gets script lua from an external shared library at
							<filename>/home/rodrigo/projects/c++/ConfigAsteriskLib/ConfigAsterisk.so</filename>
						</para>
                </description>                
        </application>        
 ***/


 
static int getLuaScript_exec( struct ast_channel *chan, const char *psz_variable );
/*static char * getLuaScript_exec( const char * psz_variable );*/

static char * GetLuaScript_app = "GetLuaScript";

static int load_module(void)
{
	if ( ast_register_application_xml(GetLuaScript_app, getLuaScript_exec) ) 
	{
		return AST_MODULE_LOAD_FAILURE;
	}
 
	return AST_MODULE_LOAD_SUCCESS;
}


static int unload_module(void)
{
	int ret = ast_unregister_application(GetLuaScript_app);
	
	return ret;

}

static int getLuaScript_exec(struct ast_channel *chan, const char *psz_variable)
{	
	void *lib_handle;
	char * (*fn)(const char *);	
	char *error;
	char szScriptLua[20000];	
	char *pszScriptLua;	
	
	pszScriptLua = &szScriptLua[0];

	ast_log(LOG_NOTICE, "[GetLuaScript]: variable : %s \n", psz_variable);
	memset(szScriptLua,0x00,sizeof(szScriptLua));
	lib_handle = dlopen("/home/rodrigo/projects/c++/ConfigAsteriskLib/ConfigAsterisk.so", RTLD_LAZY);
	if (!lib_handle)
	{
		ast_log(LOG_NOTICE, "[GetLuaScript]: Error %s\n", dlerror() );		
		return 0;
	}

	fn = dlsym(lib_handle, "getLuaScript");
	if ((error = dlerror()) != NULL) 
	{
		ast_log(LOG_NOTICE, "[GetLuaScript]: Error %s\n", error);		
		return 0;
	}

	pszScriptLua = (*fn)(psz_variable);	
	
	if ( pszScriptLua != NULL ) 
	{
		if ( strlen(pszScriptLua) < sizeof(szScriptLua) )
		{
			ast_log(LOG_NOTICE, "[AppConfigAsterisk] returned SCRIPTLUA  : %s\r\n", pszScriptLua);
			pbx_builtin_setvar_helper(chan, "scriptLua", pszScriptLua); 
		}
		else
		{
			ast_log(LOG_NOTICE, "szScriptLua size greater then sizeof(szScriptLua) \r\n");
		}
	} else
		ast_log(LOG_NOTICE, "pszScriptLua returned is NULL !!! \r\n");
	
	dlclose(lib_handle);
	
	
	/* return pszScriptLua; */
	return 0;
}

AST_MODULE_INFO_STANDARD(ASTERISK_GPL_KEY, "Asterisk Database Configuration Reader module");

