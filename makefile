CC=gcc -m64 -Wall -ansi 
CFLAGS=-I. -ldl

TARGET = AppConfigAsterisk

all: $(TARGET)


$(TARGET): AppConfigAsterisk.o 
	$(CC) -o $(TARGET) AppConfigAsterisk.o $(CFLAGS)
	
clean:
	$(RM) $(TARGET)
	rm -rf *.o